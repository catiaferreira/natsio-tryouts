name := "natsio-tryouts"

version := "0.1"

scalaVersion := "2.13.1"

resolvers += "Sonatype releases" at "https://oss.sonatype.org/content/repositories/releases"

resolvers += "Sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

libraryDependencies += "io.nats" % "java-nats-streaming" % "2.2.3"

mainClass in (Compile, run) := Some("com.natsio.Main")