package com.natsio

import java.nio.charset.StandardCharsets
import java.util.Date

import io.nats.streaming.{Message, StreamingConnection, StreamingConnectionFactory, Subscription, SubscriptionOptions}

object Main {

  def main(args: Array[String]): Unit = {
    val connectionFactory = new StreamingConnectionFactory("test-cluster", "bar")

    val connection = connectionFactory.createConnection()

    connection.publish("testing", ("Trying Nats" + new Date().toString).getBytes(StandardCharsets.UTF_8))

    val sub1 = PotatoesSubscriber("testing", connection).subscribe
    val sub2 = AllSubscriber("testing", connection).subscribe

    Thread.sleep(1000)
    sub1.unsubscribe()
    sub2.unsubscribe()
    connection.close()
  }
}

case class PotatoesSubscriber(subject: String, connection: StreamingConnection) {

  def subscribe: Subscription = {
    connection.subscribe(subject, (message: Message) => {
      println("POTATOE GOT A MESSAGE with subject: " + message.getSubject)
      println("and data: " + new String(message.getData))
    }, new SubscriptionOptions.Builder().startWithLastReceived().build())
  }
}

case class AllSubscriber(subject: String, connection: StreamingConnection){
  def subscribe: Subscription = {
    connection.subscribe(subject, (message: Message) => {
      println("ALL THE MESSAGES GOT A MESSAGE with subject: " + message.getSubject)
      println("and data: " + new String(message.getData))
    }, new SubscriptionOptions.Builder().deliverAllAvailable().build())
  }
}